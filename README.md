# docker-home-automation

## Installation
### Install Docker

Always begin with update & upgrade
`sudo apt-get update && sudo apt-get -y upgrade`

Download and run docker install script
`wget -q -O - https://get.docker.com/ | sh`

Add user to docker group, to run docker without sudo
`sudo usermod -aG docker pi`

I needed reboot after that, to start docker process. Should not be needed?
`sudo reboot`

Test docker
`docker run hello-world`


### Install Docker Compose
Install pip
`sudo apt-get -y install python-pip`

Install docker-compose
`sudo pip install docker-compose`


### Deploy
`sudo git clone https://gitlab.com/jocke.g/docker-home-automation.git /opt/docker-home-automation`

`sudo git clone https://gitlab.com/jocke.g/docker-home-automation-secrets.git /opt/docker-home-automation-secrets`

`cd /opt/docker-home-automation/`


Deploy to Swarm:
`docker stack deploy -c docker-compose.yml home-automation`

Inspiration:
https://www.reddit.com/r/homeassistant/comments/895iw6/my_home_assistant_setup_rpi_3b_docker_compose/
